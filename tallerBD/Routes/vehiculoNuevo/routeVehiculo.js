const express = require('express');
const modelVehiculos = require('../../Models/modelVehiculos');

let app = express();
//inicio metodo post
app.post('/nuevo/vehiculo', (req, res) => {
    let body = req.body;
    console.log(body);
    let newSchemaVehiculo = new modelVehiculos({
        nombreDuenio: body.nombreDuenio,
        apellidoPaternoDuenio: body.apellidoPaternoDuenio,
        apellidoMaternoDuenio: body.apellidoMaternoDuenio,
        marcaVehiculo: body.marcaVehiculo,
        submarcaVehiculo: body.submarcaVehiculo,
        colorVehiculo: body.colorVehiculo,
        numeroPuertas: body.numeroPuertas
    });

    newSchemaVehiculo
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Guardados con éxito, papu...',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );


});
//Fin metodo post
//inicio metodo get

app.get('/todos/autos', async(req, res) =>{
    await modelVehiculos
        .find()
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Encontrados con éxito, papu...',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

//Fin metodo get
//inicio metodo put

//fin metodo put
//inicio metodo delete

//fin metodo delete


module.exports = app;