const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let vehiculoNuevo = new Schema({
    nombreDuenio: { type: String },
    apellidoPaternoDuenio: { type: String },
    apellidoMaternoDuenio: { type: String },
    marcaVehiculo: { type: String },
    submarcaVehiculo: { type: String },
    colorVehiculo: { type: String },
    numeroPuertas: { type: Number }
});


module.exports = mongoose.model('vehiculo', vehiculoNuevo);