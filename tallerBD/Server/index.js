//Librerias
const bodyParser = require('body-parser');
const express = require('express');

//Iniciar Express
const app = express();


//Activar Cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

//Midlewares de express
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('../Routes/index'))

module.exports = app;